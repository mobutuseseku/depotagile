class AddQuantityToLineItem < ActiveRecord::Migration
  def self.up
    add_column :line_items, :quantity, :integer, :default => 1

    LineItem.find(:all).each do |lineitem|
      lineitem.update_attribute :quantity, lineitem.product.quantity
    end
  end

  def self.down
  	remove_column :line_items, :quantity
  end
end
